* @ValidationCode : MjotMjExMDQ0MjU2OkNwMTI1MjoxNTk1NTIyNTkxNzM5Ok1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 23 Jul 2020 19:43:11
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE EB.Demo
*
* Implementation of EB.Demo.GetParam
*
*
FUNCTION GET.DEMO.PARAM
    rec = EB.Demo.DemoParam.CacheRead("SYSTEM", err)
RETURN(rec)
END