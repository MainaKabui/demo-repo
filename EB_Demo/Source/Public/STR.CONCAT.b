* @ValidationCode : MjoxNDIyMzI0OTAzOkNwMTI1MjoxNTk1NTIwMjM4Nzk3Ok1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 23 Jul 2020 19:03:58
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE EB.Demo
*
* Implementation of EB.Demo.Concat
*
* str1(IN) :
* str2(IN) :
*
FUNCTION STR.CONCAT(str1, str2)
    str3 = str1:str2
RETURN(str3)
END