* @ValidationCode : MjoxNDE1MDgzMzM1OkNwMTI1MjoxNTk1NTM4NzQyNzUyOk1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 24 Jul 2020 00:12:22
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE EB.Demo

SUBROUTINE V.CHECK.RTN
    $USING EB.SystemTables
    $USING FT.Contract
    $USING EB.LocalReferences

    orderingBank = EB.Demo.BankName
    txnType = EB.Demo.TransType
    paramRec = EB.Demo.GetParam()
    EB.Demo.setParamRec(paramRec)
                   
    EB.SystemTables.setRNew(FT.Contract.FundsTransfer.OrderingBank, orderingBank)
    EB.SystemTables.setRNew(FT.Contract.FundsTransfer.TransactionType, txnType)
    channelRef = EB.SystemTables.getRNew(FT.Contract.FundsTransfer.LocalRef)
    
    appName = "FUNDS.TRANSFER"
    fieldName = "CHANNEL.REF"
    EB.LocalReferences.GetLocRef(appName, fieldName, pos)
    channelRef<1,pos> = "REF10001"
    EB.SystemTables.setRNew(FT.Contract.FundsTransfer.LocalRef, channelRef)
                
END
