* @ValidationCode : Mjo5MjA1OTQ2Mjc6Q3AxMjUyOjE1OTU1MzAwOTEyODU6TWFpbmE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMTlfU1AyNC4wOi0xOi0x
* @ValidationInfo : Timestamp         : 23 Jul 2020 21:48:11
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE EB.Demo

SUBROUTINE V.INPUT.RTN
    $USING EB.SystemTables
    $USING EB.ErrorProcessing
    $USING FT.Contract

    modulo = EB.Demo.Modulo
    creditAmt = EB.SystemTables.getRNew(FT.Contract.FundsTransfer.CreditAmount)
    IF MOD(creditAmt, modulo) NE 0 THEN
        EB.SystemTables.setEtext("Enter an even number amount")
        EB.ErrorProcessing.StoreEndError()
        EB.SystemTables.setAf(FT.Contract.FundsTransfer.CreditAmount)
        RETURN
    END
      
RETURN

END
